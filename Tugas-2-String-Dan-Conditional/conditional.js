// Tugas if-else
var nama = "john"
var peran = "warewolf"

if(nama == "" && peran == "") {
    console.log("Nama harus diisi!")
}else if(peran == '') {
    console.log('Halo ' + nama + ', pilih peranmu untuk memulai game!')
}else if(peran == 'penyihir' ) {
    console.log('Selamat datang di dunia Warewolf, ' + nama)
    console.log('Halo Penyihir ' + nama + ', Kamu dapat melihat siapa yang menjadi warewolf!')
}else if(peran == 'Guard') {
    console.log('Selamat datang di Dunia Warewolf, ' + nama)
    console.log('Halo Guard ' + nama + ', Kamu akan membantu melindungi temanmu dari serangan warewolf')
}else if(peran == 'warewolf') {
    console.log('Selamat datang di dunia warewolf, ' + nama)
    console.log('Halo warewolf ' + nama + ', Kamu akan memakan mangsa setiap malam!') 
}
// Akhir Tugas if-else 

// Tugas Switch Case
var Hari = 21;
var Bulan = 12;
var Tahun = 1945;

switch(Bulan) {
    case 1:Bulan = 'Januari'; break;
    case 2:Bulan = 'februari'; break;
    case 3:Bulan = 'Maret'; break;
    case 4:Bulan = 'April'; break;
    case 5:Bulan = 'Mei'; break;
    case 6:Bulan = 'Juni'; break;
    case 7:Bulan = 'Juli'; break;
    case 8:Bulan = 'Agustus'; break;
    case 9:Bulan = 'September'; break;
    case 10:Bulan = 'Oktober'; break;
    case 11:Bulan = 'November'; break;
    case 12:Bulan = 'Desember'; break;
    default:  { console.log('Masukkan hari, Bulan dan Tahun'); } 
}

console.log(Hari + ' ' + Bulan + ' ' + Tahun);

